--!nonstrict

-- src/UI/Components/TextButton.lua
-- Provides a TextButton component with default Studio theming

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Lume = require(Libraries.Lume)
local Roact = require(Libraries.Roact)

--
-- Roact contexts
--

local Theme = require(Modules.UI.Theme)

--
-- Component definition
--

local TextButton = Roact.Component:extend("TextButton")

--
-- Roact methods
--

--
-- render
--

function TextButton:render()
	return Theme.use(function(theme)
		local defaults = {
			TextColor3 = theme.ButtonText,
			BackgroundColor3 = theme.Button,
			BorderColor3 = theme.ButtonBorder,
		}
		return Roact.createElement("TextButton", Lume.merge(defaults, self.props))
	end)
end

--
-- Return component
--

return TextButton
