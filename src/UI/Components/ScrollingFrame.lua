--!nonstrict

-- src/UI/Components/ScrollingFrame.lua
-- Provides a ScrollingFrame component with default Studio theming

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Lume = require(Libraries.Lume)
local Roact = require(Libraries.Roact)

--
-- Roact contexts
--

local Theme = require(Modules.UI.Theme)

--
-- Component definition
--

local ScrollingFrame = Roact.Component:extend("ScrollingFrame")

--
-- Roact methods
--

--
-- render
--

function ScrollingFrame:render()
	return Theme.use(function(theme)
		local defaults = {
			AutomaticCanvasSize = Enum.AutomaticSize.XY,
			BackgroundColor3 = theme.MainBackground,
			BorderColor3 = theme.Border,
			ScrollBarImageColor3 = Color3.new(0,0,0),
		}
		return Roact.createElement("ScrollingFrame", Lume.merge(defaults, self.props))
	end)
end

--
-- Return component
--

return ScrollingFrame
