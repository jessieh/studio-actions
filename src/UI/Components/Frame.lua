--!nonstrict

-- src/UI/Components/Frame.lua
-- Provides a Frame component with default Studio theming

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Lume = require(Libraries.Lume)
local Roact = require(Libraries.Roact)

--
-- Roact contexts
--

local Theme = require(Modules.UI.Theme)

--
-- Component definition
--

local Frame = Roact.Component:extend("Frame")

--
-- Roact methods
--

--
-- render
--

function Frame:render()
	return Theme.use(function(theme)
		local defaults = {
			BackgroundColor3 = theme.MainBackground,
			BorderColor3 = theme.Border,
		}
		return Roact.createElement("Frame", Lume.merge(defaults, self.props))
	end)
end

--
-- Return component
--

return Frame
