--!nonstrict

-- src/UI/Components/TextLabel.lua
-- Provides a TextLabel component with default Studio theming

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Lume = require(Libraries.Lume)
local Roact = require(Libraries.Roact)

--
-- Roact contexts
--

local Theme = require(Modules.UI.Theme)

--
-- Component definition
--

local TextLabel = Roact.Component:extend("TextLabel")

--
-- Roact methods
--

--
-- render
--

function TextLabel:render()
	return Theme.use(function(theme)
		local defaults = {
			BackgroundColor3 = theme.MainBackground,
			BorderSizePixel = 0,
			TextColor3 = theme.MainText,
			AutomaticSize = Enum.AutomaticSize.XY,
		}
		return Roact.createElement("TextLabel", Lume.merge(defaults, self.props))
	end)
end

--
-- Return component
--

return TextLabel