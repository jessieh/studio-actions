--!nonstrict

-- src/UI/Components/TextBox.lua
-- Provides a TextBox component with default Studio theming

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Lume = require(Libraries.Lume)
local Roact = require(Libraries.Roact)

--
-- Roact contexts
--

local Theme = require(Modules.UI.Theme)

--
-- Component definition
--

local TextBox = Roact.Component:extend("TextBox")

--
-- Roact methods
--

--
-- render
--

function TextBox:render()
	return Theme.use(function(theme)
		local defaults = {
			AutomaticSize = Enum.AutomaticSize.XY,
			BackgroundColor3 = theme.InputFieldBackground,
			BorderColor3 = theme.InputFieldBorder,
			ClearTextOnFocus = false,
			PlaceholderColor3 = theme.DimmedText,
			TextColor3 = theme.MainText,
		}
		return Roact.createElement("TextBox", Lume.merge(defaults, self.props))
	end)
end

--
-- Return component
--

return TextBox
