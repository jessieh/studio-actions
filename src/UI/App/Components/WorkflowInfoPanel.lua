--!nonstrict

-- src/UI/App/Components/WorkflowInfoPanel.lua
-- Provides a workflow information panel Component

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Roact = require(Libraries.Roact)

--
-- Local modules
--

local ConfigStore = require(Modules.Util.ConfigStore)

--
-- Roact components
--

local Frame = require(Modules.UI.Components.Frame)
local ScrollingFrame = require(Modules.UI.Components.ScrollingFrame)
local TextLabel = require(Modules.UI.Components.TextLabel)

--
-- Helper functions
--

local function capitalize(str)
	return (string.gsub(str, "^%l", string.upper))
end

local function parseISO8601(timestamp: string): table
	local parsedTime
	-- YES I'M IGNORING THE OFFSET,
	-- NO IT'S NOT GOING TO KILL ANYONE
	string.gsub(timestamp, "(%d%d%d%d)%-(%d%d)%-(%d%d)T(%d%d):(%d%d):(%d%d)", function(year, month, day, hour, min, sec)
		parsedTime = {
			year = year,
			month = month,
			day = day,
			hour = hour,
			min = min,
			sec = sec,
		}
	end)
	return parsedTime
end

local function diffISO8601(startTime: string, endTime: string): number
	local startTimeUnix = os.time(parseISO8601(startTime))
	local endTimeUnix = os.time(parseISO8601(endTime))
	return endTimeUnix - startTimeUnix
end

local function jobStatusToEmoji(jobData: table): string
	if jobData.status ~= "completed" then
		local statuses = {
			queued = "⋯",
			in_progress = "⏲️",
			-- completed = ""
		}
		return statuses[jobData.status]
	else
		local outcomes = {
			action_required = "❔",
			cancelled = "✖️",
			failure = "❌",
			neutral = "➖",
			success = "✔️",
			skipped = "〰️",
			stale = "✖️",
			timed_out = "❌",
		}
		return outcomes[jobData.conclusion]
	end
end

local function jobStatusToText(jobData: table): string
	if jobData.status ~= "completed" then
		local statuses = {
			queued = "Queued",
			in_progress = "Running",
			-- completed = "",
		}
		return statuses[jobData.status]
	else
		local outcomes = {
			action_required = "Action Needed",
			cancelled = "Cancelled",
			failure = "Failed",
			neutral = "Neutral",
			success = "Succeeded",
			skipped = "Skipped",
			stale = "Stale",
			timed_out = "Timed Out",
		}
		return outcomes[jobData.conclusion]
	end
end

local function buildJobElements(LastRunData: table)
	local elements = {}
	for jobIndex, jobData in pairs(LastRunData) do
		local steps = {}
		for stepIndex, stepData in pairs(jobData.steps) do
			table.insert(
				steps,
				Roact.createElement(TextLabel, {
					Text = jobStatusToEmoji(stepData) .. "  " .. stepData.name,
					TextWrapped = true,
					TextXAlignment = Enum.TextXAlignment.Left,
					LayoutOrder = jobIndex + stepIndex,
				})
			)
		end
		table.insert(
			elements,
			Roact.createElement(Frame, {
				AutomaticSize = Enum.AutomaticSize.Y,
				Size = UDim2.new(1, -20, 0, 30),
				LayoutOrder = jobIndex,
			}, {
				Padding = Roact.createElement("UIPadding", {
					PaddingTop = UDim.new(0, 10),
					PaddingLeft = UDim.new(0, 10),
					PaddingRight = UDim.new(0, 10),
					PaddingBottom = UDim.new(0, 10),
				}),
				Layout = Roact.createElement("UIListLayout", {
					Padding = UDim.new(0, 10),
					FillDirection = Enum.FillDirection.Vertical,
					SortOrder = Enum.SortOrder.LayoutOrder,
				}),
				Roact.createElement(TextLabel, {
					Text = jobStatusToEmoji(jobData) .. "  " .. capitalize(jobData.name),
					TextSize = 10,
				}),
				Roact.createElement(TextLabel, {
					Text = "Status: " .. jobStatusToText(jobData),
				}),
				jobData.started_at and Roact.createElement(TextLabel, {
					-- Refusing to think about locales please don't make me
					Text = "Started: " .. os.date("%c", os.time(parseISO8601(jobData.started_at))),
				}),
				jobData.completed_at and Roact.createElement(TextLabel, {
					-- NOT THINKING ABOUT LOCALES OR TIMEZONES PLEASE DON'T MAKE ME
					Text = "Duration: " .. diffISO8601(jobData.started_at, jobData.completed_at) .. "s",
				}),
				Roact.createFragment(steps),
			})
		)
	end
	return Roact.createFragment(elements)
end

--
-- Component definition
--

local WorkflowInfoPanel = Roact.Component:extend("WorkflowInfoPanel")

--
-- Roact methods
--

--
-- render
--

function WorkflowInfoPanel:render()
	if self.props.LastRunData.error then
		return Roact.createElement(TextLabel, {
			Size = UDim2.new(1, 0, 1, 0),
			Text = self.props.LastRunData.error,
			LayoutOrder = 1,
		})
	end
	return Roact.createElement(ScrollingFrame, {
		CanvasPosition = Vector2.new(0, 0),
		Size = UDim2.new(1, 0, 1, -30),
		CanvasSize = UDim2.new(1, -4, 0, 0),
		ScrollBarThickness = 2,
		LayoutOrder = 1,
	}, {
		Padding = Roact.createElement("UIPadding", {
			PaddingTop = UDim.new(0, 10),
			PaddingLeft = UDim.new(0, 10),
			PaddingRight = UDim.new(0, 10),
			PaddingBottom = UDim.new(0, 10),
		}),
		Layout = Roact.createElement("UIListLayout", {
			Padding = UDim.new(0, 10),
			FillDirection = Enum.FillDirection.Vertical,
			SortOrder = Enum.SortOrder.LayoutOrder,
		}),
		RepoLabel = Roact.createElement(TextLabel, {
			Text = "" .. ConfigStore.GHRepoURL .. "",
			TextSize = 10,
			LayoutOrder = -2,
		}),
		WorkflowLabel = Roact.createElement(TextLabel, {
			Text = jobStatusToEmoji(self.props.LastRunData.workflowRunData) .. "  " .. self.props.LastRunData.workflowRunData.name,
			TextSize = 10,
			LayoutOrder = -1,
		}),
		self.props.LastRunData.workflowRunData.created_at and Roact.createElement(TextLabel, {
			-- Refusing to think about locales please don't make me
			Text = "Started: " .. os.date(
				"%c",
				os.time(parseISO8601(self.props.LastRunData.workflowRunData.created_at))
			),
		}),
		self.props.LastRunData.workflowRunData.updated_at and Roact.createElement(TextLabel, {
			-- NOT THINKING ABOUT LOCALES OR TIMEZONES PLEASE DON'T MAKE ME
			Text = "Duration: " .. diffISO8601(
				self.props.LastRunData.workflowRunData.created_at,
				self.props.LastRunData.workflowRunData.updated_at
			) .. "s",
		}),
		buildJobElements(self.props.LastRunData.jobRunData),
	})
end

--
-- Return component
--

return WorkflowInfoPanel
