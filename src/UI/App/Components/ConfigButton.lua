--!nonstrict

-- src/UI/App/Components/ConfigButton.lua
-- Provides Config Button component that toggles visibility of the Config widget

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Lume = require(Libraries.Lume)
local Roact = require(Libraries.Roact)

--
-- Roact contexts
--

local Widgets = require(Modules.UI.Widgets)

--
-- Roact components
--

local TextButton = require(Modules.UI.Components.TextButton)

--
-- Component definition
--

local ConfigButton = Roact.Component:extend("WorkflowInfoPanel")

--
-- Roact methods
--

--
-- render
--

function ConfigButton:render()
	return Widgets.use(function(widgets)
		local defaults = {
			Text = "⚙️",
			[Roact.Event.Activated] = function()
				widgets.config.Enabled = not widgets.config.Enabled
			end,
		}
		return Roact.createElement(TextButton, Lume.merge(defaults, self.props))
	end)
end

--
-- Return component
--

return ConfigButton
