--!nonstrict

-- src/UI/App/init.lua
-- Primary App component (root component)

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Base64 = require(Libraries.Base64)
local Roact = require(Libraries.Roact)

--
-- Local modules
--

local ConfigStore = require(Modules.Util.ConfigStore)

--
-- Roblox services
--

local HttpService = game:GetService("HttpService")

--
-- Roact components
--

local Frame = require(Modules.UI.Components.Frame)
local TextButton = require(Modules.UI.Components.TextButton)

local ConfigButton = require(Modules.UI.App.Components.ConfigButton)
local WorkflowInfoPanel = require(Modules.UI.App.Components.WorkflowInfoPanel)

--
-- Helper functions
--

local function triggerWorkflow(workflowID: number): nil
	local repoURL = ConfigStore.GHRepoURL or ""
	local accessToken = ConfigStore.GHAccessToken or ""

	local credential = Base64.encode(":" .. accessToken)

	pcall(function()
		HttpService:RequestAsync({
			Url = "https://api.github.com/repos/" .. repoURL .. "/actions/workflows/" .. workflowID .. "/dispatches",
			Method = "POST",
			Headers = {
				Accept = "application/vnd.github.v3+json",
				Authorization = "Basic " .. credential,
				["Content-Type"] = "application/json",
			},
			Body = HttpService:JSONEncode({ ref = "main" }), -- TODO: Don't hardcode this...
		})
	end)
end

local function getWorkflowRunInfo(): table | string
	local repoURL = ConfigStore.GHRepoURL or ""
	local username = ConfigStore.GHUsername or ""
	local accessToken = ConfigStore.GHAccessToken or ""

	if repoURL == "" or username == "" or accessToken == "" then
		return {
			error = [[Please configure Studio Actions.
			
			(Use the "⚙️" button!)]],
		}
	end

	local credential = Base64.encode(":" .. accessToken)

	local latestRunData
	pcall(function()
		local response = HttpService:RequestAsync({
			Url = "https://api.github.com/repos/" .. repoURL .. "/actions/runs?per_page=1",
			Method = "GET",
			Headers = {
				Accept = "application/vnd.github.v3+json",
				Authorization = "Basic " .. credential,
			},
		})
		local responseJSON = HttpService:JSONDecode(response.Body)
		latestRunData = responseJSON.workflow_runs[1]
	end)

	if not latestRunData then
		return {
			error = [[Failed to retrieve workflow run status.
			
			Make sure that:
			
			• Your repo URL is correct
			• Your credentials are correct
			• HttpService is enabled]],
		}
	end

	if not latestRunData.id then
		return {
			error = [[No workflow runs found.
			
			Have you run any jobs yet?"]],
		}
	end

	local jobRunData
	pcall(function()
		local response = HttpService:RequestAsync({
			Url = "https://api.github.com/repos/" .. repoURL .. "/actions/runs/" .. latestRunData.id .. "/jobs",
			Method = "GET",
			Headers = {
				Accept = "application/vnd.github.v3+json",
				Authorization = "Basic " .. credential,
			},
		})
		local responseJSON = HttpService:JSONDecode(response.Body)
		jobRunData = responseJSON.jobs
	end)

	if not jobRunData then
		return {
			error = "No job data found.",
		}
	end

	return {
		workflowRunData = latestRunData,
		jobRunData = jobRunData,
	}
end

--
-- Component definition
--

local App = Roact.Component:extend("App")

--
-- Roact methods
--

--
-- init
--

function App:init()
	self:setState({
		lastRunData = {
			error = "Getting workflow status...",
		},
	})
	task.spawn(function()
		self:setState({
			lastRunData = getWorkflowRunInfo(),
		})
	end)
end

--
-- didMount
--

function App:didMount()
	self.updating = true
	task.spawn(function()
		while self.updating do
			self:setState({
				lastRunData = getWorkflowRunInfo(),
			})
			if
				self.state.lastRunData
				and self.state.lastRunData
				and self.state.lastRunData.workflowRunData
				and self.state.lastRunData.status ~= "completed"
			then
				task.wait(3)
			else
				task.wait(30)
			end
		end
	end)
end

--
-- willUnmount
--

function App:willUnmount()
	self.updating = false
end

--
-- render
--

function App:render()
	return Roact.createElement(Frame, {
		Size = UDim2.new(1, 0, 1, 0),
		Position = UDim2.new(0, 0, 0, 0),
	}, {
		Layout = Roact.createElement("UIListLayout", {
			FillDirection = Enum.FillDirection.Vertical,
			SortOrder = Enum.SortOrder.LayoutOrder,
		}),
		ButtonRow = Roact.createElement(Frame, {
			Size = UDim2.new(1, 0, 0, 30),
			BorderSizePixel = 1,
			LayoutOrder = 0,
		}, {
			Layout = Roact.createElement("UIListLayout", {
				FillDirection = Enum.FillDirection.Horizontal,
				SortOrder = Enum.SortOrder.LayoutOrder,
			}),
			ConfigButton = Roact.createElement(ConfigButton, {
				Size = UDim2.new(0.10, 0, 0, 30),
				LayoutOrder = 0,
			}),
			RefreshButton = Roact.createElement(TextButton, {
				Text = "🔄 Refresh",
				Size = UDim2.new(0.45, 0, 0, 30),
				[Roact.Event.Activated] = function()
					self:setState({
						lastRunData = {
							error = "Getting workflow status...",
						},
					})
					task.spawn(function()
						self:setState({
							lastRunData = getWorkflowRunInfo(),
						})
					end)
				end,
				LayoutOrder = 1,
			}),
			RunButton = Roact.createElement(TextButton, {
				Active = self.state.lastRunData ~= nil and not self.state.lastRunData.error,
				AutoButtonColor = self.state.lastRunData ~= nil and not self.state.lastRunData.error,
				Text = "▶️ Trigger",
				Size = UDim2.new(0.45, 0, 0, 30),
				LayoutOrder = 2,

				[Roact.Event.Activated] = function()
					triggerWorkflow(self.state.lastRunData.workflowRunData.workflow_id)
					self:setState({
						lastRunData = {
							error = "Triggering workflow run...",
						},
					})
					task.spawn(function()
						task.wait(3)
						self:setState({
							lastRunData = getWorkflowRunInfo(),
						})
					end)
				end,
			}),
		}),
		WorkflowInfo = Roact.createElement(WorkflowInfoPanel, {
			LastRunData = self.state.lastRunData,
		}),
	})
end

--
-- Return component
--

return App
