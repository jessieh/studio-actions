--!nonstrict

-- src/UI/Widgets.lua
-- Provides a Provider component to host primary UI components in a context with
-- references to the widget instances, and a consumer function that consumes the context

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent
local Libraries = Root.Libraries

--
-- Library modules
--

local Roact = require(Libraries.Roact)

--
-- Context definition
--

local WidgetContext = Roact.createContext()

--
-- Component definition
--

local WidgetProvider = Roact.Component:extend("ThemeProvider")

--
-- Roact methods
--

--
-- render
--

function WidgetProvider:render()
	return Roact.createElement(WidgetContext.Provider, {
		value = self.props.widgets,
	}, self.props[Roact.Children])
end

--
-- Local functions
--

--
-- provide
--

-- Wrap `children` with the Provider component of the widgets context,
-- setting the Provider's value to `widgets`
local function provide(widgets, children)
	return Roact.createElement(WidgetProvider, {
		widgets = widgets,
	}, children)
end

--
-- use
--

-- Wrap `renderFn` with the Consumer component of the widgets context
local function use(renderFn)
	return Roact.createElement(WidgetContext.Consumer, {
		render = renderFn,
	})
end

--
-- Return Provider component and consumer function
--

return {
	provide = provide,
	use = use,
}
