--!strict

-- src/UI/Theme.lua
-- Offers a provider function to host primary UI components in a context with
-- theme information, and a consumer function that consumes the context

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent
local Libraries = Root.Libraries

--
-- Library modules
--

local Roact = require(Libraries.Roact)

--
-- Context definition
--

local ThemeContext = Roact.createContext()

--
-- Component definition
--

local ThemeProvider = Roact.Component:extend("ThemeProvider")

--
-- Custom methods
--

--
-- updateTheme
--

function ThemeProvider:updateTheme()
	local studioTheme = settings().Studio.Theme

	local themeTable = {}
	for _, enumItem in pairs(Enum.StudioStyleGuideColor:GetEnumItems()) do
		themeTable[enumItem.Name] = studioTheme:GetColor(enumItem)
	end

	self:setState({
		theme = themeTable,
	})
end

--
-- Roact methods
--

--
-- init
--

function ThemeProvider:init()
	self:updateTheme()
end

--
-- didMount
--

function ThemeProvider:didMount()
	self.connection = settings():GetService("Studio").ThemeChanged:Connect(function()
		self:updateTheme()
	end)
end

--
-- willUnmount
--

function ThemeProvider:willUnmount()
	self.connection:Disconnect()
end

--
-- render
--

function ThemeProvider:render()
	return Roact.createElement(ThemeContext.Provider, {
		value = self.state.theme,
	}, self.props[Roact.Children])
end

--
-- Local functions
--

--
-- provide
--

-- Wrap `children` with the Provider component of the theme context
local function provide(children)
	return Roact.createElement(ThemeProvider, nil, children)
end

--
-- use
--

-- Wrap `renderFn` with the Consumer component of the theme context
local function use(renderFn)
	return Roact.createElement(ThemeContext.Consumer, {
		render = renderFn,
	})
end

--
-- Return Provider component and consumer function
--

return {
	provide = provide,
	use = use,
}
