--!nonstrict

-- src/UI/Config/init.lua
-- Primary Config component (root component)

--
-- Local module roots
--

local Root = script.Parent.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Roact = require(Libraries.Roact)

--
-- Local modules
--

local ConfigStore = require(Modules.Util.ConfigStore)

--
-- Roact components
--

local Frame = require(Modules.UI.Components.Frame)
local TextBox = require(Modules.UI.Components.TextBox)
local TextButton = require(Modules.UI.Components.TextButton)
local TextLabel = require(Modules.UI.Components.TextLabel)

--
-- Component definition
--

local Config = Roact.Component:extend("Config")

--
-- Roact methods
--

--
-- init
--

function Config:init()
	self:setState({
		GHRepoURL = ConfigStore.GHRepoURL or "",
		GHUsername = ConfigStore.GHUsername or "",
		GHAccessToken = ConfigStore.GHAccessToken or "",
	})
end

--
-- render
--

function Config:render()
	return Roact.createElement(Frame, {
		Size = UDim2.new(1, 0, 1, 0),
		Position = UDim2.new(0, 0, 0, 0),
	}, {
		Padding = Roact.createElement("UIPadding", {
			PaddingTop = UDim.new(0, 15),
			PaddingLeft = UDim.new(0, 15),
			PaddingRight = UDim.new(0, 15),
			PaddingBottom = UDim.new(0, 15),
		}),
		Layout = Roact.createElement("UIListLayout", {
			FillDirection = Enum.FillDirection.Vertical,
			SortOrder = Enum.SortOrder.LayoutOrder,
			Padding = UDim.new(0, 5),
		}),
		TitleLabel = Roact.createElement(TextLabel, {
			Text = "Actions Config",
			TextSize = 12,
			Size = UDim2.new(1, 0, 0, 45),
			LayoutOrder = 0,
		}),
		GHRepoURLLabel = Roact.createElement(TextLabel, {
			Text = "🔗 GitHub Repo",
			Size = UDim2.new(1, 0, 0, 30),
			LayoutOrder = 1,
		}),
		GHRepoURLInput = Roact.createElement(TextBox, {
			Text = self.state.GHRepoURL,
			PlaceholderText = "(Ex: rojo-rbx/rojo)",
			Size = UDim2.new(1, 0, 0, 30),
			LayoutOrder = 2,
			
			[Roact.Change.Text] = function(textBox)
				self:setState({
					GHRepoURL = textBox.Text,
				})
			end,
		}),
		GHAccessTokenLabel = Roact.createElement(TextLabel, {
			Text = "🔑 Access Token",
			Size = UDim2.new(1, 0, 0, 30),
			LayoutOrder = 5,
		}),
		GHAccessTokenInput = Roact.createElement(TextBox, {
			Text = self.state.GHAccessToken,
			PlaceholderText = "(Ex: abc_023bn03289lkaf3n2890j0n30naljr12zx9a)",
			Size = UDim2.new(1, 0, 0, 30),
			LayoutOrder = 6,
			
			[Roact.Change.Text] = function(textBox)
				self:setState({
					GHAccessToken = textBox.Text,
				})
			end,
		}),
		ButtonRow = Roact.createElement(Frame, {
			Size = UDim2.new(1, 0, 0, 40),
			BorderSizePixel = 0,
			LayoutOrder = 7,
		}, {
			Padding = Roact.createElement("UIPadding", {
				PaddingTop = UDim.new(0, 15),
			}),
			Layout = Roact.createElement("UIListLayout", {
				FillDirection = Enum.FillDirection.Horizontal,
				SortOrder = Enum.SortOrder.LayoutOrder,
				Padding = UDim.new(0, 15),
			}),
			CancelButton = Roact.createElement(TextButton, {
				Text = "↩️ Reset",
				Size = UDim2.new(0.5, -7.5, 0, 30),
				LayoutOrder = 1,
				
				[Roact.Event.Activated] = function()
					self:setState({
						GHRepoURL = ConfigStore.GHRepoURL or "",
						GHUsername = ConfigStore.GHUsername or "",
						GHAccessToken = ConfigStore.GHAccessToken or "",
					})
				end,
			}),
			RunButton = Roact.createElement(TextButton, {
				Text = "✅ Apply",
				Size = UDim2.new(0.5, -7.5, 0, 30),
				LayoutOrder = 2,
				
				[Roact.Event.Activated] = function()
					ConfigStore.GHRepoURL = self.state.GHRepoURL
					ConfigStore.GHUsername = self.state.GHUsername
					ConfigStore.GHAccessToken = self.state.GHAccessToken
				end,
			}),
		}),
	})
end

--
-- Return component
--

return Config
