--!nonstrict

-- src/Utils/ConfigStore.lua
-- Provides a data model for storing plugin configuration values

--
-- Constants
--

local acceptedKeys = {
    GHRepoURL = true,
    GHUsername = true,
    GHAccessToken = true,
}

--
-- Module definition
--

local _ConfigStore = {
    _pluginRef = nil,
}

local ConfigStore = {}

--
-- Metamethods
--

--
-- __index
--

function _ConfigStore.__index(_, key)
    if not _ConfigStore._pluginRef then
        error("Attempted to index ConfigStore before initialization!")
    end
    if acceptedKeys[key] then
        return _ConfigStore._pluginRef:GetSetting(key)
    end
    error("Invalid key requested from ConfigStore: " .. key)
end

--
-- __newindex
--

function _ConfigStore.__newindex(_, key, value)
    if not _ConfigStore._pluginRef then
        error("Attempted to index ConfigStore before initialization!")
    end
    if acceptedKeys[key] then
        return _ConfigStore._pluginRef:SetSetting(key, value)
    end
    error("Invalid key provided to ConfigStore: " .. key)
end

--
-- Module methods
--

--
-- init
--

function ConfigStore.init(pluginRef)
    _ConfigStore._pluginRef = pluginRef
end

--
-- Return module
--

setmetatable(ConfigStore, _ConfigStore)

return ConfigStore
