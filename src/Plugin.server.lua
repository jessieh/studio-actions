--!nonstrict

-- src/Plugin.server.lua
-- Studio Actions plugin source code

plugin.Name = "Studio Actions"

--
-- Local module roots
--

local Root = script.Parent.Parent
local Libraries = Root.Libraries
local Modules = Root.Plugin

--
-- Library modules
--

local Roact = require(Libraries.Roact)

--
-- Local modules
--

local ConfigStore = require(Modules.Util.ConfigStore)

--
-- Roact contexts
--

local Theme = require(Modules.UI.Theme)
local Widgets = require(Modules.UI.Widgets)

--
-- Roact components
--

local App = require(Modules.UI.App)
local Config = require(Modules.UI.Config)

--
-- Roblox services
--

local TestService = game:GetService("TestService")

--
-- Plugin code
--

do
	--
	-- Initialize ConfigStore
	--

	ConfigStore.init(plugin)

	--
	-- Primary plugin widget
	--

	-- Create the primary plugin widget
	local pluginWidget = plugin:CreateDockWidgetPluginGui(
		"StudioActions",
		DockWidgetPluginGuiInfo.new(
			Enum.InitialDockState.Left,
			false -- Widget will be initially hidden
		)
	)
	pluginWidget.Title = "Workflow Viewer"

	--
	-- Config widget
	--

	local configWidgetWidth = 400
	local configWidgetHeight = 260
	local configWidget = plugin:CreateDockWidgetPluginGui(
		"StudioActionsConfig",
		DockWidgetPluginGuiInfo.new(
			Enum.InitialDockState.Float,
			false, -- Widget will be initially hidden
			true, -- Widget enabled state will always be overridden on plugin load
			configWidgetWidth,
			configWidgetHeight,
			configWidgetWidth,
			configWidgetHeight
		)
	)
	configWidget.Title = "Studio Actions Config"

	--
	-- Mount Roact component trees
	--

	-- Mount the App component to the primary plugin widget
	local pluginWidgetRoactInstance = Roact.mount(
		Widgets.provide({
			plugin = pluginWidget,
			config = configWidget,
		}, {
			Theme.provide({
				Roact.createElement(App),
			}),
		}),
		pluginWidget
	)

	-- Mount the Config component to the config widget
	local configWidgetRoactInstance = Roact.mount(
		Widgets.provide({
			plugin = pluginWidget,
			config = configWidget,
		}, {
			Theme.provide({
				Roact.createElement(Config),
			}),
		}),
		configWidget
	)

	--
	-- Toolbar
	--

	-- Create a "Studio Actions" section on the toolbar
	local toolbar = plugin:CreateToolbar("Studio Actions")

	-- Add a toolbar button to toggle the primary plugin widget
	local toggleWidgetButton = toolbar:CreateButton(
		"Workflow Viewer",
		"Open the Studio Actions workflow viewer",
		"rbxassetid://8859331360"
	)
	toggleWidgetButton.ClickableWhenViewportHidden = true
	toggleWidgetButton:SetActive(pluginWidget.Enabled)

	-- Add a toolbar button to trigger Studio's built-in test service
	local runTestsButton = toolbar:CreateButton(
		"Run Tests",
		"Trigger TestService for the current project",
		"rbxassetid://8859331755"
	)
	runTestsButton.ClickableWhenViewportHidden = true
	runTestsButton.Click:Connect(function()
		runTestsButton:SetActive(false)
		TestService:Run()
	end)

	--
	-- Toggle behavior
	--

	-- Function for toggling primary plugin widget
	local toggleWidgetFn = function()
		pluginWidget.Enabled = not pluginWidget.Enabled
		toggleWidgetButton:SetActive(pluginWidget.Enabled)
	end

	-- Attach plugin widget creation/toggling behavior to toolbar button and close event
	toggleWidgetButton.Click:Connect(toggleWidgetFn)
	pluginWidget:BindToClose(toggleWidgetFn)

	--
	-- Unloading behavior
	--

	-- Define plugin unloading behavior
	plugin.Unloading:Connect(function()
		Roact.unmount(pluginWidgetRoactInstance)
		Roact.unmount(configWidgetRoactInstance)
	end)
end
