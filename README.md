# <img src="./assets/workflows-icon.png" width=70> Studio Actions

A Roblox Studio plugin that provides a suite of tools for running tests and interacting with Github Actions workflows

## Building

Building the .rbxm:

`.\build.cmd`

Build directly to Plugins folder (default `%LOCALAPPDATA%\Roblox\Plugins`) and watch for changes:

`.\serve.cmd`

## Using

TODO: Document configuration here
